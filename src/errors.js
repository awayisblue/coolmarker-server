let commonErrCode = 2000
let businessErrCode = 3000
module.exports = {
  commonStatus: {
    '200': {
      statusCode: 200,
      data: {
        code: 0,
        msg: 'OK'
      }
    },
    '301': {
      statusCode: 301,
      data: {
        code: 301,
        msg: 'Moved Permanently'
      }
    },
    '302': {
      statusCode: 302,
      data: {
        code: 302,
        msg: 'Redirecting'
      }
    },
    '404': {
      statusCode: 404,
      data: {
        code: 404,
        msg: 'Not Found'
      }
    },
    '403': {
      statusCode: 403,
      data: {
        code: 403,
        msg: 'Forbidden'
      }
    }
  },
  commonError: {
    OK: {
      statusCode: 200,
      data: {
        code: 0,
        msg: 'OK'
      }
    },
    PARAM_ERROR: {
      statusCode: 400,
      data: {
        code: commonErrCode++,
        msg: '参数错误'
      }
    },
    SERVER_ERROR: {
      statusCode: 500,
      data: {
        code: commonErrCode++,
        msg: '服务器错误'
      }
    }
  },
  businessError: {
    ERROR: {
      statusCode: 400,
      data: {
        code: businessErrCode++,
        msg: '错误'
      }
    }
  }
}

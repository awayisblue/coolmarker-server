const mysql = require('../../mysql')
const Sequelize = require('sequelize')
const Foo = mysql.define('foo', {
  // instantiating will automatically set the flag to true if not set
  flag: { type: Sequelize.BOOLEAN, allowNull: false, defaultValue: true },

  // default values for dates => current time
  myDate: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },

  // setting allowNull to false will add NOT NULL to the column, which means an error will be
  // thrown from the DB when the query is executed if the column is null. If you want to check that a value
  // is not null before querying the DB, look at the validations section below.
  title: { type: Sequelize.STRING, allowNull: false }
}, {
  tableName: 'foo',
  timestamps: false
})
module.exports = Foo

const mysql = require('../../mysql')
const Sequelize = require('sequelize')
const Bar = mysql.define('bar', {
  id: {type: Sequelize.INTEGER, primaryKey: true},
  // instantiating will automatically set the flag to true if not set
  flag: { type: Sequelize.BOOLEAN, allowNull: false, defaultValue: true },

  // default values for dates => current time
  myDate: { type: Sequelize.DATE, defaultValue: Sequelize.NOW },

  // setting allowNull to false will add NOT NULL to the column, which means an error will be
  // thrown from the DB when the query is executed if the column is null. If you want to check that a value
  // is not null before querying the DB, look at the validations section below.
  title: { type: Sequelize.STRING, allowNull: false },
  fooId: {type: Sequelize.INTEGER, allowNull: false}
}, {
  tableName: 'bar',
  timestamps: false
})
// Bar.belongsTo(Foo, {foreignKey: 'fooId'})
module.exports = Bar

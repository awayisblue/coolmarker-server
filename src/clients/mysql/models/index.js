const Foo = require('./Foo')
const Bar = require('./Bar')
Foo.hasMany(Bar, {foreignKey: 'fooId', targetKey: 'id'})
Bar.belongsTo(Foo, {foreignKey: 'fooId'})
module.exports = {
  Foo,
  Bar
}

const Router = require('koa-router')
const router = new Router()
const {Foo} = require('../../clients/mysql/models')
// 支持单页面
router.get('/', async function (ctx, next) {
  let foo = await Foo.find()
  let bars = await foo.getBars()
  ctx.body = {
    foo,
    bars
  }
})
module.exports = router

const Router = require('koa-router')
const root = require('./root/route')
const config = require('config')
const router = new Router({prefix: config.server.prefix || '/'})
router.use(root.routes())
module.exports = router
